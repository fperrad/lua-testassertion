
# lua-TestAssertion

---

## Overview

It is an extension of
[lua-TestMore](https://fperrad.frama.io/lua-TestMore).

It provides many Lua friendly assertions.

It uses the
[Test Anything Protocol](https://en.wikipedia.org/wiki/Test_Anything_Protocol)
as output, that allows a compatibility with the Perl QA ecosystem.
For example,
[prove](https://metacpan.org/dist/Test-Harness/view/bin/prove)
a basic CLI.

## Status

lua-TestAssertion is in beta stage.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-TestAssertion source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-testassertion).

## Installation

The easiest way to install lua-TestAssertion is to use LuaRocks:

```sh
luarocks install lua-testassertion
```

or manually, with:

```sh
make install
```

## Copyright and License

Copyright &copy; 2021-2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
