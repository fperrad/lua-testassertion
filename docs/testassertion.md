
# Test.Assertion

---

# Reference

All functions are injected in the global environment `_G`.

#### plan( arg )

```lua
plan(2)

pass "one"
pass "two"
```

gives

```text
1..2
ok 1 - one
ok 2 - two
```

#### done_testing( num_tests )

```lua
plan 'no_plan'

pass "one"
pass "two"
done_testing()
```

gives

```text
ok 1 - one
ok 2 - two
1..2
```

#### skip_all( reason )

```lua
if everything_looks_good then
    plan(7)
else
    skip_all "looks bad"
end
```

gives

```text
1..0 # SKIP looks bad
```

#### BAIL_OUT( reason )

```lua
plan(7)

if not require_ok 'MyApp' then
    BAIL_OUT "no MyApp"
end
```

gives

```text
1..7
not ok 1 - require 'MyApp'
#     module 'MyApp' not found:
#       no field package.preload['MyApp']
#       no file '.\MyApp.lua'
#       ...
Bail out!  no MyApp
```

and breaks the execution of `prove`.

#### subtest( name, func )

```lua
plan(3)
pass "First test"
subtest('An example subtest', function ()
    plan(2)
    pass "This is a subtest"
    pass "So is this"
end)
pass "Third test"
```

gives

```text
1..3
ok 1 - First test
# Subtest: An example subtest
    1..2
    ok 1 - This is a subtest
    ok 2 - So is this
ok 2 - An example subtest
ok 3 - Third test
```

#### falsy( val [, name] )
#### truthy( val [, name] )

#### is_false( val [, name] )
#### is_not_false( val [, name] )
#### is_true( val [, name] )
#### is_not_true( val [, name] )

#### is_boolean( val [, name] )
#### is_not_boolean( val [, name] )
#### is_function( val [, name] )
#### is_not_function( val [, name] )
#### is_nil( val [, name] )
#### is_not_nil( val [, name] )
#### is_number( val [, name] )
#### is_not_number( val [, name] )
#### is_string( val [, name] )
#### is_not_string( val [, name] )
#### is_table( val [, name] )
#### is_not_table( val [, name] )
#### is_thread( val [, name] )
#### is_not_thread( val [, name] )
#### is_userdata( val [, name] )
#### is_not_userdata( val [, name] )

#### is_float( val [, name] )
#### is_integer( val [, name] )

#### equal( got, expected [, name] )
#### equals( got, expected [, name] )
#### not_equal( got, expected [, name] )
#### not_equals( got, expected [, name] )

#### array_equal( got, expected [, name] )
#### array_equals( got, expected [, name] )

#### same( got, expected [, name] )

#### near( got, expected, tolerance [, name] )

#### is_eq( this, that [, name] )
#### is_ge( this, that [, name] )
#### is_gt( this, that [, name] )
#### is_le( this, that [, name] )
#### is_lt( this, that [, name] )
#### is_ne( this, that [, name] )

#### contain( got, pattern [, name] )
#### contains( got, pattern [, name] )
#### not_contain( got, pattern [, name] )
#### not_contains( got, pattern [, name] )

#### match( got, pattern [, name] )
#### matches( got, pattern [, name] )
#### not_match( got, pattern [, name] )
#### not_matches( got, pattern [, name] )

#### error_equal( code [, params_array], expected [, name] )
#### error_equals( code [, params_array], expected [, name] )

#### not_error( code [, params_array] [, name] )
#### not_errors( code [, params_array] [, name] )

#### error_match( code [, params_array], pattern [, name] )
#### error_matches( code [, params_array], pattern [, name] )
#### match_error( code [, params_array], pattern [, name] )
#### matches_error( code [, params_array], pattern [, name] )

#### dump( val )

#### diag( msg )

#### note( msg )

#### skip( reason [, count] )

```lua
plan(4)

pass "one"

if true then
    skip("here, segfault", 2)
else
    fail "two"
    fail "three"
end

pass "four"
```

gives

```text
1..4
ok 1 - one
ok 2 - # skip here, segfault
ok 3 - # skip here, segfault
ok 4 - four
```

#### todo_skip( reason [, count] )

```lua
plan(3)

pass "one"

if true then
    todo_skip "here, segfault"
else
    fail "two"
end

pass "three"
```

gives

```text
1..3
ok 1 - one
not ok 2 - # TODO & SKIP here, segfault
ok 3 - three
```

#### skip_rest( reason )

```lua
plan(3)

if not require_ok 'MyApp' then
    skip_rest "no MyApp"
    os.exit()
end

pass "two"
pass "three"
```

gives

```text
1..3
not ok 1 - require 'MyApp'
#     module 'MyApp' not found:
#       no field package.preload['MyApp']
#       no file '.\MyApp.lua'
#       ...
ok 2 - # skip no MyApp
ok 3 - # skip no MyApp
```

#### todo( reason [, count] )

```lua
plan(4)

pass "one"

todo( "not yet implemented", 2 )
fail "two"
fail "three"

pass "four"
```

gives

```text
1..4
ok 1 - one
not ok 2 - two # TODO # not yet implemented
not ok 3 - three # TODO # not yet implemented
ok 4 - four
```

# Examples

```lua
-- 99-example.lua

require 'Test.Assertion'

plan(9)

is_true(true, "true")
truthy(1, "1 is true")
is_false(false, "false")
falsy(nil, "nil is false")

equals(1 + 1, 2, "addition")

matches("with aaa", 'a', "pattern matches")
not_matches("with aaa", 'b', "pattern doesn't match")

error_matches([[error 'MSG']], '^[^:]+:%d+: MSG', "loadstring error")
error_equals(error, { 'MSG' }, 'MSG', "function error with param")
```

```text
$ lua 99-example.lua
1..9
ok 1 - true
ok 2 - 1 is true
ok 3 - false
ok 4 - nil is false
ok 5 - addition
ok 6 - pattern matches
ok 7 - pattern doesn't match
ok 8 - loadstring error
ok 9 - function error with param
```

Now, with [prove](https://metacpan.org/dist/Test-Harness/view/bin/prove).

```text
$ prove --exec=lua 99-example.lua
99-example.lua .. ok
All tests successful.
Files=1, Tests=9,  0 wallclock secs ( 0.05 usr +  0.20 sys =  0.25 CPU)
Result: PASS
```

If your continuous integration tool requires the JUnix XML format.

```xml
$ prove --exec=lua --formatter=TAP::Formatter::JUnit 99-example.lua
<testsuites>
  <testsuite failures="0"
             errors="0"
             tests="9"
             name="test_99example_t">
    <testcase name="1 - true"></testcase>
    <testcase name="2 - 1 is true"></testcase>
    <testcase name="3 - false"></testcase>
    <testcase name="4 - nil is false"></testcase>
    <testcase name="5 - addition"></testcase>
    <testcase name="6 - pattern matches"></testcase>
    <testcase name="7 - pattern doesn't match"></testcase>
    <testcase name="8 - loadstring error"></testcase>
    <testcase name="9 - function error with param"></testcase>
    <system-out><![CDATA[1..9
ok 1 - true
ok 2 - 1 is true
ok 3 - false
ok 4 - nil is false
ok 5 - addition
ok 6 - pattern matches
ok 7 - pattern doesn't match
ok 8 - loadstring error
ok 9 - function error with param
]]></system-out>
    <system-err></system-err>
  </testsuite>
</testsuites>
```

If your results must be stored first, and processed after.

```text
$ lua 99-example.lua > 99-example.tap
$ prove --source=TAP::Parser::SourceHandler::RawTAP 99-example.tap
99-example.tap .. ok
All tests successful.
Files=1, Tests=9,  0 wallclock secs ( 0.02 usr +  0.04 sys =  0.06 CPU)
Result: PASS
```
