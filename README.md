
lua-TestAssertion
=================

Introduction
------------

It is an extension of [lua-TestMore](https://fperrad.frama.io/lua-TestMore/).

It provides many Lua friendly assertions.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-testassertion/>,
and the sources are hosted at <https://framagit.org/fperrad/lua-testassertion/>).

Copyright and License
---------------------

Copyright (c) 2021-2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

