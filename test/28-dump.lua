#! /usr/bin/lua

require 'Test.Assertion'
require 'Test.Builder.Tester'
plan(6)

test_diag [[3.14]]
dump( 3.14 )
test_test "dump number"


test_diag [["foo"]]
dump( 'foo' )
test_test "dump string"


test_diag [[false]]
dump( false )
test_test "dump boolean"


test_diag [[nil]]
dump( nil )
test_test "dump nil"


test_diag [[{]]
test_diag [[  [1] = 1,]]
test_diag [[  [2] = 2,]]
test_diag [[  [3] = {]]
test_diag [[    [1] = 3.1,]]
test_diag [[    [2] = 3.2,]]
test_diag [[    [3] = {]]
test_diag [[      [1] = "3.3.1",]]
test_diag [[    },]]
test_diag [[  },]]
test_diag [[  [4] = 4,]]
test_diag [[}]]
dump( { 1, 2, { 3.1, 3.2, { '3.3.1' } }, 4 } )
test_test "dump array"


test_diag [[{]]
test_diag [[  a = {]]
test_diag [[    b = {]]
test_diag [[      c = true,]]
test_diag [[    },]]
test_diag [[  },]]
test_diag [[}]]
dump( { a = { b = { c = true } } } )
test_test "dump hash"
