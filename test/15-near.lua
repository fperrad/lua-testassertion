
require 'Test.Builder.Tester'
require 'Test.Assertion'
plan(5)

test_out "ok 1 - 3.14 is near 3.1416"
near( 3.14, 3.1416, 0.01, "3.14 is near 3.1416" )
test_test "3.14 is near 3.1416"

test_out "not ok 1 - true is not near 3.1416"
test_fail(2)
test_diag [[got isn't a number : true]]
near( true, 3.1416, 0.01, "true is not near 3.1416" )
test_test "got is not number"

test_out "not ok 1 - 3.14 is not near false"
test_fail(2)
test_diag [[expected isn't a number : false]]
near( 3.14, false, 0.01, "3.14 is not near false" )
test_test "expected is not number"

test_out "not ok 1 - 3.14 is not near 3.1416"
test_fail(2)
test_diag [[tolerance isn't a number : foo]]
near( 3.14, 3.1416, 'foo', "3.14 is not near 3.1416" )
test_test "tolerance is not number"

test_out "not ok 1 - 3.14 is not near 3.1416"
test_fail(3)
test_diag [[         got: 3.14]]
test_diag [[    expected: 3.1416 +/- 0.0001]]
near( 3.14, 3.1416, 0.0001, "3.14 is not near 3.1416" )
test_test "3.14 is not near 3.1416"

