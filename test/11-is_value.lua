
require 'Test.Builder.Tester'
require 'Test.Assertion'
plan(12)

test_out "ok 1 - true is true"
is_true( true, "true is true" )
test_test "true is true"

test_out "not ok 1 - 1 is not true"
test_fail(3)
test_diag [[         got: 1]]
test_diag [[    expected: true]]
is_true( 1, "1 is not true" )
test_test "1 is not true"

test_out "ok 1 - false is false"
is_false( false, "false is false" )
test_test "false is false"

test_out "not ok 1 - 0 is not false"
test_fail(3)
test_diag [[         got: 0]]
test_diag [[    expected: false]]
is_false( 0, "0 is not false" )
test_test "0 is not false"

test_out "ok 1 - nil is nil"
is_nil( nil, "nil is nil" )
test_test "nil is nil"

test_out "not ok 1 - false is not nil"
test_fail(3)
test_diag [[         got: false]]
test_diag [[    expected: nil]]
is_nil( false, "false is not nil" )
test_test "false is not nil"


test_out "ok 1 - 1 is not true"
is_not_true( 1, "1 is not true" )
test_test "1 is not true"

test_out "not ok 1 - true is true"
test_fail(3)
test_diag [[         got: true]]
test_diag [[    expected: anything else]]
is_not_true( true, "true is true" )
test_test "true is true"

test_out "ok 1 - 0 is not false"
is_not_false( 0, "0 is not false" )
test_test "0 is not false"

test_out "not ok 1 - false is false"
test_fail(3)
test_diag [[         got: false]]
test_diag [[    expected: anything else]]
is_not_false( false, "false is false" )
test_test "false is false"

test_out "ok 1 - false is not nil"
is_not_nil( false, "false is not nil" )
test_test "false is not nil"

test_out "not ok 1 - nil is nil"
test_fail(3)
test_diag [[         got: nil]]
test_diag [[    expected: anything else]]
is_not_nil( nil, "nil is nil" )
test_test "nil is nil"

