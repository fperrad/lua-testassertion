
require 'Test.More'
plan(74)

if not require_ok 'Test.Assertion' then
    BAIL_OUT "no lib"
end

local m = require 'Test.Assertion'
type_ok( m, 'table' )
note( m._COPYRIGHT )
like( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
note( m._DESCRIPTION )
like( m._DESCRIPTION, 'assertion', "_DESCRIPTION" )
diag( m._VERSION )
like( m._VERSION, '^%d%.%d%.%d$', "_VERSION" )

type_ok( m.plan, 'function', 'plan' )
type_ok( m.done_testing, 'function', 'done_testing' )
type_ok( m.skip_all, 'function', 'skip_all' )
type_ok( m.BAIL_OUT, 'function', 'BAIL_OUT' )
type_ok( m.subtest, 'function', 'subtest' )
type_ok( m.diag, 'function', 'diag' )
type_ok( m.note, 'function', 'note' )
type_ok( m.skip, 'function', 'skip' )
type_ok( m.todo_skip, 'function', 'todo_skip' )
type_ok( m.skip_rest, 'function', 'skip_rest' )
type_ok( m.todo, 'function', 'todo' )

type_ok( m.is_boolean, 'function', 'is_boolean' )
type_ok( m.is_cdata, 'function', 'is_cdata' )
type_ok( m.is_function, 'function', 'is_function' )
type_ok( m.is_number, 'function', 'is_number' )
type_ok( m.is_string, 'function', 'is_string' )
type_ok( m.is_table, 'function', 'is_table' )
type_ok( m.is_thread, 'function', 'is_thread' )
type_ok( m.is_userdata, 'function', 'is_userdata' )

type_ok( m.is_not_boolean, 'function', 'is_not_boolean' )
type_ok( m.is_not_cdata, 'function', 'is_not_cdata' )
type_ok( m.is_not_function, 'function', 'is_not_function' )
type_ok( m.is_not_number, 'function', 'is_not_number' )
type_ok( m.is_not_string, 'function', 'is_not_string' )
type_ok( m.is_not_table, 'function', 'is_not_table' )
type_ok( m.is_not_thread, 'function', 'is_not_thread' )
type_ok( m.is_not_userdata, 'function', 'is_not_userdata' )

type_ok( m.is_float, 'function', 'is_float' )
type_ok( m.is_integer, 'function', 'is_integer' )

type_ok( m.is_nil, 'function', 'is_nil' )
type_ok( m.is_not_nil, 'function', 'is_not_nil' )

type_ok( m.is_false, 'function', 'is_false' )
type_ok( m.is_true, 'function', 'is_true' )

type_ok( m.is_not_false, 'function', 'is_not_false' )
type_ok( m.is_not_true, 'function', 'is_not_true' )

type_ok( m.is_eq, 'function', 'is_eq' )
type_ok( m.is_ge, 'function', 'is_ge' )
type_ok( m.is_gt, 'function', 'is_gt' )
type_ok( m.is_le, 'function', 'is_le' )
type_ok( m.is_lt, 'function', 'is_lt' )
type_ok( m.is_ne, 'function', 'is_ne' )

type_ok( m.near, 'function', 'near' )

type_ok( m.falsy, 'function', 'falsy' )
type_ok( m.truthy, 'function', 'truthy' )

type_ok( m.equal, 'function', 'equal' )
type_ok( m.equals, 'function', 'equals' )
type_ok( m.not_equal, 'function', 'not_equal' )
type_ok( m.not_equals, 'function', 'not_equals' )

type_ok( m.match, 'function', 'match' )
type_ok( m.matches, 'function', 'matches' )
type_ok( m.not_match, 'function', 'not_match' )
type_ok( m.not_matches, 'function', 'not_matches' )

type_ok( m.contain, 'function', 'contain' )
type_ok( m.contains, 'function', 'contains' )
type_ok( m.not_contain, 'function', 'not_contain' )
type_ok( m.not_contains, 'function', 'not_contains' )

type_ok( m.array_equal, 'function', 'array_error' )
type_ok( m.array_equals, 'function', 'array_errors' )

type_ok( m.same, 'function', 'same' )

type_ok( m.error_equal, 'function', 'error_equal' )
type_ok( m.error_equals, 'function', 'error_equals' )

type_ok( m.not_error, 'function', 'not_error' )
type_ok( m.not_errors, 'function', 'not_errors' )

type_ok( m.error_match, 'function', 'error_match' )
type_ok( m.error_matches, 'function', 'error_matches' )
type_ok( m.match_error, 'function', 'match_error' )
type_ok( m.matches_error, 'function', 'matches_error' )

type_ok( m.passes, 'function', 'passes' )

type_ok( m.fails, 'function', 'fails' )
