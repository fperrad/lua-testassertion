
require 'Test.Builder.Tester'
require 'Test.Assertion'
plan(4)

test_out "ok 1 - 1 is number"
is_number( 1, "1 is number" )
test_test "1 is number"

test_out "not ok 1 - true is not number"
test_fail(2)
test_diag [[    true isn't a number, it's a boolean]]
is_number( true, "true is not number" )
test_test "true is not number"


test_out "ok 1 - true is not number"
is_not_number( true, "true is not number" )
test_test "true is not number"

test_out "not ok 1 - 1 is number"
test_fail(3)
test_diag [[    got type: 1 is number]]
test_diag [[    expected: anything else]]
is_not_number( 1, "1 is number" )
test_test "1 is number"
