#! /usr/bin/lua

require 'Test.Assertion'
require 'Test.Builder.Tester'
plan(8)


-- In there
test_out "ok 1 - What's in my dog food?"
contains( "Dog food", "foo", "What's in my dog food?" )
test_test "a small string matches"


-- Not in there
test_out "not ok 1 - Any nachos?"
test_fail(5)
test_diag [[    searched: "Dog food"]]
test_diag [[  can't find: "Nachos"]]
test_diag [[        LCSS: "o"]]
test_diag [[LCSS context: "Dog food"]]
contains( "Dog food", "Nachos", "Any nachos?" )
test_test "Substring doesn't match"


-- Source string nil
test_out "not ok 1 - Look inside nil"
test_fail(2)
test_diag [[got isn't a string : nil]]
contains( nil, "Orange everything", "Look inside nil")
test_test "Source string nil fails"


-- Searching string nil
test_out "not ok 1 - Look for nil"
test_fail(2)
test_diag [[pattern isn't a string : nil]]
contains( '"Mesh" is not a color', nil, "Look for nil" )
test_test "Substring nil fails"


-- In there
test_out "ok 1 - Any chocolate in my peanut butter?"
not_contains( "Reese's Peanut Butter Cups", "Chocolate", "Any chocolate in my peanut butter?" )
test_test "Lacking"


-- Not in there
test_out "not ok 1 - Any peanut butter in my chocolate?"
test_fail(4)
test_diag [[    searched: "Reese's Peanut Butter Cups"]]
test_diag [[   and found: "Peanut Butter"]]
test_diag [[ at position: 9 (line 1 column 9)]]
not_contains( "Reese's Peanut Butter Cups", "Peanut Butter", "Any peanut butter in my chocolate?" )
test_test "Not lacking"


-- Source string nil
test_out "not ok 1 - Look inside nil"
test_fail(2)
test_diag [[got isn't a string : nil]]
not_contains( nil, "Orange everything", "Look inside nil" )
test_test "Source string nil fails"


-- Searching string nil
test_out "not ok 1 - Look for nil"
test_fail(2)
test_diag [[pattern isn't a string : nil]]
not_contains( '"Fishnet" is not a color', nil, "Look for nil" )
test_test "Substring nil fails"
