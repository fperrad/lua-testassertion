
require 'Test.Assertion'

plan(9)

is_true(true, "true")
truthy(1, "1 is true")
is_false(false, "false")
falsy(nil, "nil is false")

equals(1 + 1, 2, "addition")

matches("with aaa", 'a', "pattern matches")
not_matches("with aaa", 'b', "pattern doesn't match")

error_matches([[error 'MSG']], '^[^:]+:%d+: MSG', "loadstring error")
error_equals(error, { 'MSG' }, 'MSG', "function error with param")
