#! /usr/bin/lua

require 'Test.Assertion'
require 'Test.Builder.Tester'
plan(15)


test_out "ok 1 - function error with param"
error_equals(error, { 'MSG' }, 'MSG', "function error with param")
test_test "ok error_is"


local e = setmetatable({ msg = 'MSG'}, { __tostring = function (t) return t.msg end })
test_out "ok 1 - function error with object error"
error_equals(error, { e }, 'MSG', "function error with object error")
test_test "ok error_is"


test_out "not ok 1 - function error with param"
test_fail(3)
test_diag "         got: bad"
test_diag "    expected: MSG"
error_equals(error, { 'bad' }, 'MSG', "function error with param")
test_test "fail error_is (wrong error)"


test_out "not ok 1 - function print without param"
test_fail(3)
test_diag "    unexpected success"
test_diag "    expected: MSG"
error_equals(print, 'MSG', "function print without param")
test_test "fail error_is (unexpected success)"


test_out "not ok 1 - can't compile"
test_fail(3)
test_diag [[    can't compile code :]]
test_diag [[    [string "?syntax error?"]:1: unexpected symbol near '?']]
error_equals([[?syntax error?]], 'MSG', "can't compile")
test_test "fail error_is (can't compile)"


test_out "ok 1 - loadstring error"
error_matches([[error 'MSG']], '^[^:]+:%d+: MSG', "loadstring error")
test_test "ok error_like"


test_out "ok 1 - function error with object error"
error_matches(error, { e }, '^MSG$', "function error with object error")
test_test "ok error_like"


test_out "not ok 1 - loadstring error"
test_fail(3)
test_diag [[                  '[string "error 'bad'"]:1: bad']]
test_diag [[    doesn't match '^[^:]+:%d+: MSG']]
error_matches([[error 'bad']], '^[^:]+:%d+: MSG', "loadstring error")
test_test "fail error_like (doesn't match)"


test_out "not ok 1 - loadstring ok"
test_fail(3)
test_diag "    unexpected success"
test_diag "    expected: ^[^:]+:%d+: MSG"
error_matches([[m = _G]], '^[^:]+:%d+: MSG', "loadstring ok")
test_test "fail error_like (unexpected success)"


test_out "not ok 1 - can't compile"
test_fail(3)
test_diag [[    can't compile code :]]
test_diag [[    [string "?syntax error?"]:1: unexpected symbol near '?']]
error_matches([[?syntax error?]], '^[^:]+:%d+: MSG', "can't compile")
test_test "fail error_like (can't compile)"


test_out "not ok 1 - bad pattern"
test_fail(2)
test_diag [[pattern isn't a string : true]]
error_matches([[error 'bad']], true, "bad pattern")
test_test "fail error_like (bad pattern)"


test_out "ok 1 - anonymous function"
not_errors(function () return true end, "anonymous function")
test_test "ok lives_ok"


test_out "ok 1 - anonymous function"
not_errors(function (prm) return prm end, { true }, "anonymous function")
test_test "ok lives_ok"


test_out "not ok 1 - anonymous function"
test_fail(2)
test_diag("    " .. arg[0] .. ":97: MSG")
not_errors(function () error 'MSG' end, "anonymous function")
test_test "fail lives_ok"


test_out "not ok 1 - can't compile"
test_fail(3)
test_diag [[    can't compile code :]]
test_diag [[    [string "?syntax error?"]:1: unexpected symbol near '?']]
not_errors([[?syntax error?]], "can't compile")
test_test "fail lives_ok (can't compile)"

