
require 'Test.Builder.Tester'
require 'Test.Assertion'
plan(2)

test_out "ok 1 - 1 is lt 2"
is_lt( 1, 2, "1 is lt 2" )
test_test "1 is lt 2"

test_out "not ok 1 - 1 is not gt 2"
test_fail(4)
test_diag [[    1]]
test_diag [[        >]]
test_diag [[    2]]
is_gt( 1, 2, "1 is not gt 2" )
test_test "1 is not gt 2"

