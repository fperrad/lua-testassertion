
require 'Test.Builder.Tester'
require 'Test.Assertion'
plan(6)

test_out "ok 1 - 1 is integer"
is_integer( 1, "1 is integer" )
test_test "1 is integer"

test_out "not ok 1 - true is not integer"
test_fail(2)
test_diag [[    true isn't a integer, it's a boolean]]
is_integer( true, "true is not integer" )
test_test "true is not integer"

test_out "not ok 1 - 3.14 is not integer"
test_fail(2)
test_diag [[    3.14 isn't a integer, it's a float]]
is_integer( 3.14, "3.14 is not integer" )
test_test "3.14 is not integer"

test_out "ok 1 - 3.14 is float"
is_float( 3.14, "3.14 is float" )
test_test "3.14 is float"

test_out "not ok 1 - '3.14' is not float"
test_fail(2)
test_diag [[    3.14 isn't a float, it's a string]]
is_float( '3.14', "'3.14' is not float" )
test_test "'3.14' is not float"

test_out "not ok 1 - 1 is not float"
test_fail(2)
test_diag [[    1 isn't a float, it's a integer]]
is_float( 1, "1 is not float" )
test_test "1 is not float"
