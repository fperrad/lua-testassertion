#! /usr/bin/lua

require 'Test.Assertion'
require 'Test.Builder.Tester'
plan(10)


test_out "ok 1 - 42 is 42"
equals( 42, 42, "42 is 42" )
test_test "ok is"


test_out "not ok 1 - is 24 42"
test_fail(3)
test_diag [[         got: 24]]
test_diag [[    expected: 42]]
equals( 24, 42, "is 24 42" )
test_test "fail is"


test_out "ok 1 - 24 isn't 42"
not_equals( 24, 42, "24 isn't 42" )
test_test "ok isnt"


test_out "not ok 1 - isn't 42 42"
test_fail(3)
test_diag [[         got: 42]]
test_diag [[    expected: anything else]]
not_equals( 42, 42, "isn't 42 42" )
test_test "fail isnt"


test_out "ok 1 - foo is foo"
equals( "foo", "foo", "foo is foo" )
test_test "two small strings equal"


test_out "not ok 1 - foo is foo"
test_fail(4)
test_diag [[         got: "bar"]]
test_diag [[    expected: "foo"]]
test_diag [[    strings begin to differ at char 1 (line 1 column 1)]]
equals( "bar", "foo", "foo is foo" )
test_test "two small strings different"


test_out "not ok 1 - long binary strings"
test_fail(4)
test_diag [[         got: "This is a long string that will be truncated by th"...]]
test_diag [[    expected: "\000\001foo\010bar"]]
test_diag [[    strings begin to differ at char 1 (line 1 column 1)]]
equals(
    "This is a long string that will be truncated by the display() function",
    "\0\001foo\nbar",
    "long binary strings"
)
test_test "display of long strings and of control chars"


test_out "not ok 1 - spelling"
test_fail(4)
test_diag [[         got: "Element"]]
test_diag [[    expected: "El\233ment"]]
test_diag [[    strings begin to differ at char 3 (line 1 column 3)]]
equals(
    "Element",
    "El�ment",
    "spelling"
)
test_test "Escape high-ascii chars"


test_out "not ok 1 - foo\\nfoo is foo\\nfoo"
test_fail(4)
test_diag [[         got: "foo\010foo"]]
test_diag [[    expected: "foo\010fpo"]]
test_diag [[    strings begin to differ at char 6 (line 2 column 2)]]
equals( "foo\nfoo", "foo\nfpo", "foo\\nfoo is foo\\nfoo" )
test_test "Count correctly prefix with multiline strings"


test_out "not ok 1 - this isn't Ulysses"
test_fail(4)
test_diag [[         got: ..."he bowl aloft and intoned:\010--Introibo ad altare de"...]]
test_diag [[    expected: ..."he bowl alift and intoned:\010--Introibo ad altare de"...]]
test_diag [[    strings begin to differ at char 233 (line 4 column 17)]]
equals( [[
Stately, plump Buck Mulligan came from the stairhead, bearing a bowl of
lather on which a mirror and a razor lay crossed. A yellow dressinggown,
ungirdled, was sustained gently behind him by the mild morning air. He
held the bowl aloft and intoned:
--Introibo ad altare dei.
]], [[
Stately, plump Buck Mulligan came from the stairhead, bearing a bowl of
lather on which a mirror and a razor lay crossed. A yellow dressinggown,
ungirdled, was sustained gently behind him by the mild morning air. He
held the bowl alift and intoned:
--Introibo ad altare dei.
]], "this isn't Ulysses" )
test_test "Display offset in diagnostics"
